import express from "express"
import Controller from "../controllers/nguoidung"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.put("/update/info", Controller.updateWithoutQuyen)
router.delete("/remove/:username", Controller.remove)
router.get("/username/:username", Controller.get)
router.get("/all", Controller.getAll)

export default router