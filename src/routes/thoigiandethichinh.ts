import express from "express"
import Controller from "../controllers/thoigiandethichinh"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.delete("/remove/:id", Controller.remove)
router.get("/all", Controller.getAll)
router.get("/sinhvien/:sinhvien_id", Controller.getAllOnSinhvien)

export default router