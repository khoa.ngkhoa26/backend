import { NguoiDungPassword } from "../../models/nguoidung"
import ServiceNguoiDung from "../../services/nguoidung"

test("Cập nhập NguoiDung không thay đổi quyền", async () => {
  let sv: ServiceNguoiDung = new ServiceNguoiDung()
  sv.nguoidung_account = "hoang1"
  sv.nguoidung_password = "hoang1"
  sv.nguoidung_displayname = "hoàng"
  const result: boolean = await sv.updateWithoutQuyen()
  expect(result).not.toBe(false)
})

test("Lấy password từ NguoiDung", async () => {
  let sv: ServiceNguoiDung = new ServiceNguoiDung()
  sv.nguoidung_account = "admin"
  const result: NguoiDungPassword = await sv.getPassword()
  expect(result).not.toBe(undefined)
})