import ServiceCauHoi from "../../services/cauhoi"

test("Nhập dữ liệu vào bảng CauHoi", async () => {
  let sv: ServiceCauHoi = new ServiceCauHoi()
  sv.chuong_id = 1
  sv.cauhoi_name = "Khang đẹp trai năm nào?"
  const result = await sv.insert()
  expect(result).not.toBe(false)
})

test("Cập nhập thông tin của bảng CauHoi", async () => {
  let sv: ServiceCauHoi = new ServiceCauHoi()
  sv.cauhoi_id = 76
  sv.chuong_id = 2
  sv.cauhoi_name = "Khang đẹp trai năm nào?"
  const result = await sv.update()
  expect(result).not.toBe(false)
})

test("Xóa dữ liệu của bảng CauHoi", async() => {
  let sv: ServiceCauHoi = new ServiceCauHoi()
  sv.cauhoi_id = 76
  const result = await sv.remove()
  expect(result).not.toBe(false)
})

test("Trả về bảng CauHoi phụ thuộc vào id", async () => {
  let sv: ServiceCauHoi = new ServiceCauHoi()
  sv.cauhoi_id = 1
  const result = await sv.get()
  expect(result).not.toBe(undefined)
})

test("Trả về hết bảng CauHoi dựa trên limit và offset", async () => {
  let sv: ServiceCauHoi = new ServiceCauHoi()
  const result = await sv.getAll(10, 0)
  expect(result).not.toBe(undefined)
})

test("Trả về hết bảng CauHoi phụ thuộc vào Chuong dựa trên limit và offset", async () => {
  let sv: ServiceCauHoi = new ServiceCauHoi()
  sv.chuong_id = 1
  const result = await sv.getAllOnChuong(10, 0)
  expect(result).not.toBe(undefined)
})

test("Trả về hết bảng CauHoi phụ thuộc vào MonHoc dựa trên limit và offset", async () => {
  let sv: ServiceCauHoi = new ServiceCauHoi()
  sv.chuong_id = 1
  const result = await sv.getAllOnMonHoc(1, 10, 0)
  expect(result).not.toBe(undefined)
})

test("Trả về tổng giá trị của bảng CauHoi", async () => {
  let sv: ServiceCauHoi = new ServiceCauHoi()
  const result = await sv.getCount()
  expect(result).not.toBe(undefined)
})

test("Trả về hết bảng CauHoi phụ thuộc vào tìm kiếm dựa trên limit và offset", async () => {
  let sv: ServiceCauHoi = new ServiceCauHoi()
  const result = await sv.searchAll("Liên", 10, 0)
  expect(result).not.toBe(undefined)
})