import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceThoiGianDeThiChinh from "../services/thoigiandethichinh"
import { ArrayThoiGianDeThiChinh } from "../models/thoigiandethichinh"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const dethimonhoc_id: number = req.body.dethimonhoc_id
    const time: string = req.body.time

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1 ||
        isNaN(dethimonhoc_id) || dethimonhoc_id < 1 ||
        !time) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: id, dethimonhoc_id and time are required." })
      return
    }

    const service: ServiceThoiGianDeThiChinh = new ServiceThoiGianDeThiChinh()
    service.thoigiandethichinh_id = id
    service.dethimonhoc_id = dethimonhoc_id
    service.thoigiandethichinh_time = time
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert ThoiGianDeThiChinh successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerThoiGianDeThiChinh:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const dethimonhoc_id: number = req.body.dethimonhoc_id
    const time: string = req.body.time

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1 ||
        isNaN(dethimonhoc_id) || dethimonhoc_id < 1 ||
        !time) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: id, dethimonhoc_id and time are required." })
      return
    }

    const service: ServiceThoiGianDeThiChinh = new ServiceThoiGianDeThiChinh()
    service.thoigiandethichinh_id = id
    service.dethimonhoc_id = dethimonhoc_id
    service.thoigiandethichinh_time = time
    const result: boolean = await service.update()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update ThoiGianDeThiChinh successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerThoiGianDeThiChinh:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: id is required." })
      return
    }

    const service: ServiceThoiGianDeThiChinh = new ServiceThoiGianDeThiChinh()
    service.thoigiandethichinh_id = id
    const result: boolean = await service.remove()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Remove ThoiGianDeThiChinh successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerThoiGianDeThiChinh:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(limit) || isNaN(offset)) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both limit and offset are required." })
      return
    }

    const service: ServiceThoiGianDeThiChinh = new ServiceThoiGianDeThiChinh()
    const result: ArrayThoiGianDeThiChinh = await service.getAll(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerThoiGianDeThiChinh:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAllOnSinhvien(req: Request, res: Response): Promise<void> {
  try {
    const sinhvien_id: number = parseInt(req.params.sinhvien_id as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(sinhvien_id) || sinhvien_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both sinhvien_id is required." })
      return
    }

    const service: ServiceThoiGianDeThiChinh = new ServiceThoiGianDeThiChinh()
    const result: ArrayThoiGianDeThiChinh = await service.getAllOnSinhvien(sinhvien_id)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerThoiGianDeThiChinh:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default {
  insert, update, remove,
  getAll, getAllOnSinhvien
}