import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceLopHoc from "../services/lophoc"
import { OnlyLopHoc } from "../models/lophoc"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const name: string = req.body.name

    if (!name) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: name is required." })
      return
    }

    const service: ServiceLopHoc = new ServiceLopHoc()
    service.lophoc_name = name
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert LopHoc successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const name: string = req.body.name

    if (isNaN(id) || id < 1 || !name) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: id and name are required and chuong_id, cautraloi_id are optional." })
      return
    }

    const service: ServiceLopHoc = new ServiceLopHoc()
    service.lophoc_id = id
    service.lophoc_name = name
    const result: boolean = await service.update()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update LopHoc successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    console.log(id)

    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: id is required." })
      return
    }

    const service: ServiceLopHoc = new ServiceLopHoc()
    service.lophoc_id = id
    const result: boolean = await service.remove()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Remove LopHoc successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function get(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id, 10)

    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Id are required." })
      return
    }

    const service: ServiceLopHoc = new ServiceLopHoc()
    service.lophoc_id = id
    const result: OnlyLopHoc = await service.get()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    if (isNaN(limit) || isNaN(offset)) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: 'Bad Request: Both limit and offset are required.' })
      return
    }

    const service = new ServiceLopHoc()
    const result = await service.getAll(limit, offset)

    if (result) {
      res.json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error: Unable to fetch data.' })
    }
  } catch (error) {
    console.error('Error in ControllerLopHoc:', error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' })
  }
}

async function getCountMonhoc(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    const service = new ServiceLopHoc()
    service.lophoc_id = id
    const result = await service.getCountMonhoc()

    if (result) {
      res.json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error: Unable to fetch data.' })
    }
  } catch (error) {
    console.error('Error in ControllerLopHoc:', error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' })
  }
}

async function getCountSinhvien(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    const service = new ServiceLopHoc()
    service.lophoc_id = id
    const result = await service.getCountSinhvien()

    if (result) {
      res.json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error: Unable to fetch data.' })
    }
  } catch (error) {
    console.error('Error in ControllerLopHoc:', error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' })
  }
}

async function getOnSinhvien(req: Request, res: Response): Promise<void> {
  try {
    const sinhvien_id: number = parseInt(req.params.sinhvien_id, 10)

    if (isNaN(sinhvien_id) || sinhvien_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Id are required." })
      return
    }

    const service: ServiceLopHoc = new ServiceLopHoc()
    const result: OnlyLopHoc = await service.getOnSinhvien(sinhvien_id)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function searchAll(req: Request, res: Response): Promise<void> {
  try {
    const keyword: string = req.params.keyword
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    if (!keyword || isNaN(limit) || isNaN(offset)) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: 'Bad Request: Both keyword, limit and offset are required.' })
      return
    }

    const service = new ServiceLopHoc()
    const result = await service.searchAll(keyword, limit, offset)

    if (result) {
      res.json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error: Unable to fetch data.' })
    }
  } catch (error) {
    console.error('Error in ControllerLopHoc:', error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' })
  }
}

export default {
  insert, update, remove,
  get, getAll, getCountMonhoc, getCountSinhvien, getOnSinhvien, searchAll
}