import bcrypt from "bcrypt"
import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceNguoiDung from "../services/nguoidung"
import { ArrayNguoiDung, OnlyNguoiDung } from "../models/nguoidung"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const nguoidung_account: string = req.body.username
    const quyen_id: number = parseInt(req.body.quyen_id, 10)
    const nguoidung_password: string = req.body.password
    const nguoidung_displayname: string = req.body.displayname

    if (!nguoidung_account ||
        !nguoidung_password ||
        isNaN(quyen_id) ||
        quyen_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: Account, password and quyen_id are required."
      })
      return
    }

    const service: ServiceNguoiDung = new ServiceNguoiDung()
    service.nguoidung_account = nguoidung_account
    service.quyen_id = quyen_id
    service.nguoidung_password = await bcrypt.hash(nguoidung_password, 10)
    service.nguoidung_displayname = nguoidung_displayname
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert NguoiDung successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerNguoiDung:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function update(req: Request, res: Response): Promise<void> {
  try {
    const nguoidung_account: string = req.body.username
    const quyen_id: number = parseInt(req.body.quyen_id, 10)
    const nguoidung_password: string = req.body.password
    const nguoidung_displayname: string = req.body.displayname

    if (!nguoidung_account || !nguoidung_password || isNaN(quyen_id) ||
        quyen_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: Account, password and quyen_id are required."
      })
      return
    }

    // Lấy password trước
    const service: ServiceNguoiDung = new ServiceNguoiDung()
    service.nguoidung_account = nguoidung_account
    service.nguoidung_displayname = nguoidung_displayname
    service.quyen_id = quyen_id
    service.getPassword()
  
    // Kiểm tra password và gán
    if (nguoidung_password === service.nguoidung_password)
      service.nguoidung_password = nguoidung_password
    else
      service.nguoidung_password = await bcrypt.hash(nguoidung_password, 10)

    const result: boolean = await service.update()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update NguoiDung successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerNguoiDung:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function updateWithoutQuyen(req: Request, res: Response): Promise<void> {
  try {
    const nguoidung_account: string = req.body.username
    const nguoidung_password: string = req.body.password
    const nguoidung_displayname: string = req.body.displayname

    if (!nguoidung_account || !nguoidung_password) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: Account, password and displayname are required."
      })
      return
    }

    // Lấy password trước
    const service: ServiceNguoiDung = new ServiceNguoiDung()
    service.nguoidung_account = nguoidung_account
    service.nguoidung_displayname = nguoidung_displayname
    service.getPassword()
  
    // Kiểm tra password và gán
    if (nguoidung_password === service.nguoidung_password)
      service.nguoidung_password = nguoidung_password
    else
      service.nguoidung_password = await bcrypt.hash(nguoidung_password, 10)

    const result: boolean = await service.updateWithoutQuyen()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update NguoiDung without quyen successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerNguoiDung:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const username: string = req.params.username as string

    if (!username) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: username is required."
      })
      return
    }

    const service: ServiceNguoiDung = new ServiceNguoiDung()
    service.nguoidung_account = username
    const result: boolean = await service.remove()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Remove NguoiDung successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerNguoiDung:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function get(req: Request, res: Response): Promise<void> {
  try {
    const username: string = req.params.username as string

    // Kiểm tra dữ liệu đầu vào
    if (!username) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: username is required."
      })
      return
    }

    const service: ServiceNguoiDung = new ServiceNguoiDung()
    service.nguoidung_account = username
    const result: OnlyNguoiDung = await service.get()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerNguoiDung:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(limit) || isNaN(offset) || limit < 1 || offset < 0) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both limit and offset are required." })
      return
    }

    const service: ServiceNguoiDung = new ServiceNguoiDung()
    const result: ArrayNguoiDung = await service.getAll(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerNguoiDung:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default {
  insert, update, updateWithoutQuyen, remove,
  get, getAll
}