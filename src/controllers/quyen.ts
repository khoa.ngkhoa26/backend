import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceQuyen from "../services/quyen"
import { ArrayQuyen } from "../models/quyen"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const name: string = req.body.name

    if (!name) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Name is required." })
      return
    }

    const service: ServiceQuyen = new ServiceQuyen()
    service.quyen_name = name
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert Quyen successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerQuyen:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const name: string = req.body.name

    if (isNaN(id) || id < 1 || !name) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Id and name are required" })
      return
    }

    const service: ServiceQuyen = new ServiceQuyen()
    service.quyen_id = id
    service.quyen_name = name
    const result: boolean = await service.update()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update Quyen successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerQuyen:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Id are required." })
      return
    }

    const service: ServiceQuyen = new ServiceQuyen()
    service.quyen_id = id
    const result: boolean = await service.remove()

    if (result) {
      res.status(HttpStatus.OK).json({ message: "Remove Quyen successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerQuyen:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(limit) || isNaN(offset)) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both limit and offset are required." })
      return
    }

    const service: ServiceQuyen = new ServiceQuyen()
    const result: ArrayQuyen = await service.getAll(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerQuyen:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default { insert, update, remove, getAll }