import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceDeThiMonHoc from "../services/dethimonhoc"
import { ArrayDeThiMonHoc } from "../models/dethimonhoc"
import { LastOrCount } from "../models/alltypes"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const monhoc_id: number = req.body.monhoc_id
    const name: string = req.body.name
    const real: boolean = req.body.real
    const questions: number = req.body.questions
    const time: number = req.body.time

    if (isNaN(monhoc_id) || monhoc_id < 1 ||
        real === undefined ||
        isNaN(questions) || questions < 1 ||
        isNaN(time) || time < 1
        ) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: monhoc_id, name, real, questions and time are required." })
      return
    }

    const service: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
    service.monhoc_id = monhoc_id
    service.dethimonhoc_name = name
    service.dethimonhoc_real = real
    service.dethimonhoc_questions = questions
    service.dethimonhoc_time = time
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert DeThiMonHoc successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThiMonHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const monhoc_id: number = req.body.monhoc_id
    const name: string = req.body.name
    const real: boolean = req.body.real
    const questions: number = req.body.questions
    const time: number = req.body.time

    if (isNaN(id) || id < 1 ||
        isNaN(monhoc_id) || monhoc_id < 1 ||
        real === undefined ||
        isNaN(questions) || questions < 1 ||
        isNaN(time) || time < 1
        ) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: id, monhoc_id, name, real, questions and time are required." })
      return
    }

    const service: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
    service.dethimonhoc_id = id
    service.monhoc_id = monhoc_id
    service.dethimonhoc_name = name
    service.dethimonhoc_real = real
    service.dethimonhoc_questions = questions
    service.dethimonhoc_time = time
    const result: boolean = await service.update()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update DeThiMonHoc successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThiMonHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id, 10)

    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: id is required." })
      return
    }

    const service: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
    service.dethimonhoc_id = id
    const result: boolean = await service.remove()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Remove DeThiMonHoc successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThiMonHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    const service: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
    const result: ArrayDeThiMonHoc = await service.getAll(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThiMonHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAllOnMonhoc(req: Request, res: Response): Promise<void> {
  try {
    const monhoc_id: number = parseInt(req.params.monhoc_id, 10)
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    const service: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
    service.monhoc_id = monhoc_id
    const result: ArrayDeThiMonHoc = await service.getAllOnMonhoc(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThiMonHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAllTestOnSinhvien(req: Request, res: Response): Promise<void> {
  try {
    const sinhvien_id: number = parseInt(req.params.sinhvien_id, 10)

    const service: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
    const result: ArrayDeThiMonHoc = await service.getAllTestOnSinhvien(sinhvien_id)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThiMonHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAllRealOnSinhvien(req: Request, res: Response): Promise<void> {
  try {
    const sinhvien_id: number = parseInt(req.params.sinhvien_id, 10)

    const service: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
    const result: ArrayDeThiMonHoc = await service.getAllRealOnSinhvien(sinhvien_id)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThiMonHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getCount(req: Request, res: Response): Promise<void> {
  try {
    const service: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
    const result: LastOrCount = await service.getCount()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThiMonHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default {
  insert, update, remove,
  getAll, getAllOnMonhoc, getAllTestOnSinhvien, getAllRealOnSinhvien, getCount
}