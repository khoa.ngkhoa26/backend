import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import { JWT_SECRET_ADMIN, JWT_SECRET_USER } from "../config/auth"
import ServiceNguoiDung from "../services/nguoidung"
import ServiceQuyen from "../services/quyen"
import { OnlyQuyen } from "../models/quyen"

async function login(req: Request, res: Response): Promise<void> {
  try {
    const username: string = req.body.username
    const password: string = req.body.password

    if (!username || !password) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: username and password are required." })
      return
    }

    const service: ServiceNguoiDung = new ServiceNguoiDung()
    service.nguoidung_account = username
    const default_password: string = await service.getPassword() as string

    let isPassword: boolean = false
    if (await bcrypt.compare(password, default_password)) {
      isPassword = true
    }

    if (isPassword) {
      // Lấy dữ liệu người dùng không có displayname
      service.getWithoutDisplayname()

      // Lấy quyền
      const service_quyen: ServiceQuyen = new ServiceQuyen
      const quyen_admin: OnlyQuyen = await service_quyen.getAdmin()
      if (quyen_admin === undefined)
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error:Can't generate token!" })

      // Lấy token cho admin hay user
      let token: string = ""
      if (service.quyen_id === quyen_admin?.quyen_id)
        token = jwt.sign(
          {
            account: service.nguoidung_account,
            quyen: service.quyen_id,
            password: service.nguoidung_password
          },
          JWT_SECRET_ADMIN,
          { expiresIn: "2h" }
        )
      else
        token = jwt.sign(
          {
            account: service.nguoidung_account,
            quyen: service.quyen_id,
            password: service.nguoidung_password
          },
          JWT_SECRET_USER,
          { expiresIn: "2h" }
        )

      res.cookie("login-token", token, { httpOnly: true, maxAge: 2 * 60 * 60 * 1000, sameSite: "lax", secure: false })
      res.status(HttpStatus.OK).json({ message: "Authentication successfully!" })
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Authentication failure!" })
    }
  } catch (error) {
    console.error("Error in ControllerAuth:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function checkadmin(req: Request, res: Response): Promise<void> {
  try {
    const logintoken = req.cookies["login-token"]

    if (!logintoken) {
      res.status(HttpStatus.UNAUTHORIZED).json({ message: "User is not authenticated" })
      return
    }

    try {
      const decodedToken: any = jwt.verify(logintoken, JWT_SECRET_ADMIN)

      // Check token expiration
      const currentTimestamp = Math.floor(Date.now() / 1000) // in seconds
      if (decodedToken.exp && decodedToken.exp < currentTimestamp) {
        res.status(HttpStatus.UNAUTHORIZED).json({ message: "Token has expired" })
        return
      }

      // If decoding succeeds and token is not expired, it means it's an admin token
      res.send("User is an admin")
    } catch (adminTokenError) {
      // If decoding fails for admin token, it may be a user token
      res.send("User is not an admin")
    }
  } catch (error) {
    console.error("Error in checkadmin:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function checklogin(req: Request, res: Response): Promise<void> {
  const logintoken = req.cookies["login-token"]

  if (logintoken) {
    try {
      // Try decoding with JWT_SECRET_USER
      const decodedUserToken: any = jwt.verify(logintoken, JWT_SECRET_USER)

      // Check token expiration for user token
      const currentTimestamp = Math.floor(Date.now() / 1000) // in seconds
      if (decodedUserToken.exp && decodedUserToken.exp < currentTimestamp) {
        res.status(HttpStatus.UNAUTHORIZED).json({ message: "login-token has expired" })
        return
      }

      // If decoding succeeds and user token is not expired, it means the user token is found
      res.send("login-token is found")
    } catch (userTokenError) {
      // If decoding fails for user token, try decoding with JWT_SECRET_ADMIN
      try {
        const decodedAdminToken: any = jwt.verify(logintoken, JWT_SECRET_ADMIN)

        // Check token expiration for admin token
        const currentTimestamp = Math.floor(Date.now() / 1000) // in seconds
        if (decodedAdminToken.exp && decodedAdminToken.exp < currentTimestamp) {
          res.status(HttpStatus.UNAUTHORIZED).json({ message: "login-token has expired" })
          return
        }

        // If decoding succeeds and admin token is not expired, it means the admin token is found
        res.send("login-token is found")
      } catch (adminTokenError) {
        // If decoding fails for both user and admin tokens, it means the token is not valid
        res.send("login-token is not valid")
      }
    }
  } else {
    res.send("login-token is not found")
  }
}

async function logout(req: Request, res: Response): Promise<void> {
  try {
    // Xóa cookie
    res.clearCookie("login-token")

    // Trả về phản hồi
    res.status(HttpStatus.OK).json({ message: "Logout successfully!" })
  } catch (error) {
    console.error("Error in ControllerAuth:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default {
  login, checkadmin, checklogin, logout
}