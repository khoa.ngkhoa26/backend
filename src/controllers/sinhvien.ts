import { Request, Response } from "express"
import HttpStatus from "http-status-codes"
import ServiceSinhVien from "../services/sinhvien"
import { ArraySinhVien, OnlySinhVien } from "../models/sinhvien"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const account: string = req.body.account
    const name: string = req.body.name
    const gender: string = req.body.gender
    const birthday: string = req.body.birthday

    // Kiểm tra dữ liệu đầu vào
    if (!account || !name || !gender || !birthday) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: account, name, gender and birthday are required." })
      return
    }

    const service: ServiceSinhVien = new ServiceSinhVien()
    service.nguoidung_account = account
    service.sinhvien_name = name
    service.sinhvien_gender = gender
    service.sinhvien_birthday = birthday
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert SinhVien successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerSinhVien:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const account: string = req.body.account
    const name: string = req.body.name
    const gender: string = req.body.gender
    const birthday: string = req.body.birthday

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1 ||
        !account || !name || !gender || !birthday) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: id, account, name, gender and birthday are required." })
      return
    }

    const service: ServiceSinhVien = new ServiceSinhVien()
    service.sinhvien_id = id
    service.nguoidung_account = account
    service.sinhvien_name = name
    service.sinhvien_gender = gender
    service.sinhvien_birthday = birthday
    const result: boolean = await service.update()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update SinhVien successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerSinhVien:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: id is required." })
      return
    }

    const service: ServiceSinhVien = new ServiceSinhVien()
    service.sinhvien_id = id
    const result: boolean = await service.remove()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Remove SinhVien successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerSinhVien:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getOnAccount(req: Request, res: Response): Promise<void> {
  try {
    const username: string = req.params.username as string

    // Kiểm tra dữ liệu đầu vào
    if (!username) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: username is required." })
      return
    }

    const service: ServiceSinhVien = new ServiceSinhVien()
    const result: OnlySinhVien = await service.getOnAccount(username)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerSinhVien:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(limit) || limit < 1||
        isNaN(offset) || offset < 0) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both limit and offset are required." })
      return
    }

    const service: ServiceSinhVien = new ServiceSinhVien()
    const result: ArraySinhVien = await service.getAll(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerSinhVien:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default {
  insert, update, remove,
  getOnAccount, getAll
}