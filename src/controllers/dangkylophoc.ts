import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceDangKyLopHoc from "../services/dangkylophoc"
import { ArrayDangKyLopHoc } from "../models/dangkylophoc"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const sinhvien_id: number = req.body.sinhvien_id
    const lophoc_id: number = req.body.lophoc_id

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(sinhvien_id) || sinhvien_id < 1 ||
        isNaN(lophoc_id) || lophoc_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: sinhvien_id and lophoc_id are required." })
      return
    }

    const service: ServiceDangKyLopHoc = new ServiceDangKyLopHoc()
    service.sinhvien_id = sinhvien_id
    service.lophoc_id = lophoc_id
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert DangKyLopHoc successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDangKyLopHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const sinhvien_id: number = req.body.sinhvien_id
    const lophoc_id: number = req.body.lophoc_id

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1 ||
        isNaN(sinhvien_id) || sinhvien_id < 1 ||
        isNaN(lophoc_id) || lophoc_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: id, sinhvien_id and lophoc_id are required." })
      return
    }

    const service: ServiceDangKyLopHoc = new ServiceDangKyLopHoc()
    service.dangkylophoc_id = id
    service.sinhvien_id = sinhvien_id
    service.lophoc_id = lophoc_id
    const result: boolean = await service.update()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update DangKyLopHoc successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDangKyLopHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: id is required." })
      return
    }

    const service: ServiceDangKyLopHoc = new ServiceDangKyLopHoc()
    service.dangkylophoc_id = id
    const result: boolean = await service.remove()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Remove DangKyLopHoc successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDangKyLopHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(limit) || isNaN(offset)) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both limit and offset are required." })
      return
    }

    const service: ServiceDangKyLopHoc = new ServiceDangKyLopHoc()
    const result: ArrayDangKyLopHoc = await service.getAll(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDangKyLopHoc:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default {
  insert, update, remove,
  getAll
}