import { QueryResult } from "pg"
import pool from "../config/database"
import { ArrayChuong, Chuong, OnlyChuong } from "../models/chuong"
import { LastOrCount } from "../models/alltypes"

class ServiceChuong {
  chuong_id!: number
  monhoc_id!: number
  chuong_number!: number
  chuong_name!: string

  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_chuong($1, $2, $3);",
        values: [this.monhoc_id, this.chuong_number, this.chuong_name],
      })
    } catch (error) {
      console.log(error)
      return false
    }
    return true
  }

  async update(): Promise<boolean> {
    try {
      if (this.chuong_id !== undefined)
        await pool.query({
          text: "CALL update_chuong($1, $2, $3, $4);",
          values: [
            this.chuong_id,
            this.monhoc_id,
            this.chuong_number,
            this.chuong_name,
          ],
        })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_chuong($1);",
        values: [this.chuong_id],
      })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async getAll(limit: number, offset: number): Promise<ArrayChuong> {
    let allValues: ArrayChuong = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_chuong_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: Chuong) => ({
        chuong_id: row.chuong_id,
        monhoc_id: row.monhoc_id,
        chuong_number: row.chuong_number,
        chuong_name: row.chuong_name,
      }))
    } catch (error) {
      // Return undefine
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllChuongOnMonHoc(): Promise<ArrayChuong> {
    let allValues: ArrayChuong = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_chuong_on_monhoc($1);",
        values: [this.monhoc_id],
      });

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: Chuong) => ({
        chuong_id: row.chuong_id,
        monhoc_id: row.monhoc_id,
        chuong_name: row.chuong_name,
        chuong_number: row.chuong_number,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues;
  }

  async getCount(): Promise<LastOrCount> {
    let value: LastOrCount

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getcount_chuong();",
      })

      // result row to value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async getCountOnMonhoc(): Promise<LastOrCount> {
    let value: LastOrCount

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getcount_chuong_on_monhoc($1);",
        values: [this.monhoc_id]
      })

      // result row to value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async searchAll(keyword: string, limit: number, offset: number): 
    Promise<ArrayChuong>
  {
    let allValues: ArrayChuong = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM searchall_chuong($1, $2, $3);",
        values: [keyword as string, limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: Chuong) => ({
        chuong_id: row.chuong_id,
        monhoc_id: row.monhoc_id,
        chuong_number: row.chuong_number,
        chuong_name: row.chuong_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceChuong
