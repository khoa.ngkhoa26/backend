import { QueryResult } from "pg"

import { ArrayDeThiMonHoc, DeThiMonHoc, OnlyDeThiMonHoc } from "../models/dethimonhoc"
import pool from "../config/database"
import { LastOrCount } from "../models/alltypes"

class ServiceDeThiMonHoc {
  dethimonhoc_id!: number
  monhoc_id!: number
  dethimonhoc_name!: string
  dethimonhoc_real!: boolean
  dethimonhoc_questions!: number
  dethimonhoc_time!: number

  /* Các method cập nhập dữ liệu */
  async insert(): Promise<boolean> {
    try {
      // Execute SQL
      await pool.query({
        text: "CALL insert_dethimonhoc($1, $2, $3, $4, $5);",
        values: [
          this.monhoc_id,
          this.dethimonhoc_name,
          this.dethimonhoc_real,
          this.dethimonhoc_questions,
          this.dethimonhoc_time
        ],
      })

    } catch (error) {
      // Return undefined
      console.log(error)
      return false
    }

    return true
  }

  async update(): Promise<boolean> {
    try {
      // Execute SQL
      await pool.query({
        text: "CALL update_dethimonhoc($1, $2, $3, $4, $5, $6);",
        values: [
          this.dethimonhoc_id,
          this.monhoc_id,
          this.dethimonhoc_name,
          this.dethimonhoc_real,
          this.dethimonhoc_questions,
          this.dethimonhoc_time
        ],
      })

    } catch (error) {
      // Return undefined
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      // Execute SQL
       await pool.query({
        text: "CALL delete_dethimonhoc($1);",
        values: [this.dethimonhoc_id],
      })

    } catch (error) {
      // Return undefined
      console.log(error)
      return false
    }

    return true
  }

  /* Các method lấy dữ liệu */
  async get(): Promise<OnlyDeThiMonHoc> {
    let value: OnlyDeThiMonHoc

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_dethimonhoc_withlid($1);",
        values: [this.dethimonhoc_id],
      })

      // Get first value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async getAll(limit: number, offset: number): Promise<ArrayDeThiMonHoc> {
    let allValues: ArrayDeThiMonHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethimonhoc_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: DeThiMonHoc) => ({
        dethimonhoc_id: row.dethimonhoc_id,
        monhoc_id: row.monhoc_id,
        dethimonhoc_name: row.dethimonhoc_name,
        dethimonhoc_real: row.dethimonhoc_real,
        dethimonhoc_questions: row.dethimonhoc_questions,
        dethimonhoc_time: row.dethimonhoc_time
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllOnMonhoc(limit: number, offset: number): Promise<ArrayDeThiMonHoc> {
    let allValues: ArrayDeThiMonHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethimonhoc_on_monhoc($1, $2, $3);",
        values: [this.monhoc_id, limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: DeThiMonHoc) => ({
        dethimonhoc_id: row.dethimonhoc_id,
        monhoc_id: row.monhoc_id,
        dethimonhoc_name: row.dethimonhoc_name,
        dethimonhoc_real: row.dethimonhoc_real,
        dethimonhoc_questions: row.dethimonhoc_questions,
        dethimonhoc_time: row.dethimonhoc_time
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllTestOnSinhvien(sinhvien_id: number): Promise<ArrayDeThiMonHoc> {
    let allValues: ArrayDeThiMonHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethimonhoc_test_on_sinhvien($1);",
        values: [sinhvien_id],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: DeThiMonHoc) => ({
        dethimonhoc_id: row.dethimonhoc_id,
        monhoc_id: row.monhoc_id,
        dethimonhoc_name: row.dethimonhoc_name,
        dethimonhoc_real: row.dethimonhoc_real,
        dethimonhoc_questions: row.dethimonhoc_questions,
        dethimonhoc_time: row.dethimonhoc_time
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllRealOnSinhvien(sinhvien_id: number): Promise<ArrayDeThiMonHoc> {
    let allValues: ArrayDeThiMonHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethimonhoc_real_on_sinhvien($1);",
        values: [sinhvien_id],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: DeThiMonHoc) => ({
        dethimonhoc_id: row.dethimonhoc_id,
        monhoc_id: row.monhoc_id,
        dethimonhoc_name: row.dethimonhoc_name,
        dethimonhoc_real: row.dethimonhoc_real,
        dethimonhoc_questions: row.dethimonhoc_questions,
        dethimonhoc_time: row.dethimonhoc_time
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getCount(): Promise<LastOrCount> {
    let value: LastOrCount

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getcount_dethimonhoc();"
      })

      // Get first value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }
}

export default ServiceDeThiMonHoc