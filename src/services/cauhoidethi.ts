import { QueryResult } from "pg";

import pool from "../config/database";
import { ArrayCauHoiDeThi, CauHoiDeThi } from "../models/cauhoidethi";
import { LastOrCount } from "../models/alltypes";

class ServiceCauHoiDeThi {
  cauhoidethi_id!: number;
  dethi_id!: number;
  cauhoi_id!: number;
  cautraloi_id!: number;

  async updateCautraloi(): Promise<boolean> {
    try {
      if (this.cauhoidethi_id !== undefined)
        await pool.query({
          text: "CALL update_cauhoidethi_cautraloi($1, $2);",
          values: [this.cauhoidethi_id, this.cautraloi_id],
        });
    } catch (error) {
      console.log(error);
      return false;
    }

    return true;
  }

  async getAll(limit: number, offset: number): Promise<ArrayCauHoiDeThi> {
    let allValues: ArrayCauHoiDeThi = [];

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_cauhoidethi_withlimitoffset($1, $2);",
        values: [limit, offset],
      });

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: CauHoiDeThi) => ({
        cauhoidethi_id: row.cauhoidethi_id,
        dethi_id: row.dethi_id,
        cauhoi_id: row.cauhoi_id,
        cautraloi_id: row.cautraloi_id,
      }));
    } catch (error) {
      // Return undefined
      console.log(error);
      return undefined;
    }

    return allValues;
  }

  async getAllOnDethi(): Promise<ArrayCauHoiDeThi> {
    let allValues: ArrayCauHoiDeThi = [];

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_cauhoidethi_on_dethi($1);",
        values: [this.dethi_id],
      });

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: CauHoiDeThi) => ({
        cauhoidethi_id: row.cauhoidethi_id,
        dethi_id: row.dethi_id,
        cauhoi_id: row.cauhoi_id,
        cautraloi_id: row.cautraloi_id,
      }));
    } catch (error) {
      // Return undefined
      console.log(error);
      return undefined;
    }

    return allValues;
  }

  async getCountOnDethi(): Promise<LastOrCount> {
    let value: LastOrCount;

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getcount_cauhoidethi_on_dethi($1);",
        values: [this.dethi_id],
      });

      // result row to value
      value = result.rows[0];
    } catch (error) {
      // Return undefined
      console.log(error);
      return undefined;
    }

    return value;
  }

  async getCountTrue(): Promise<LastOrCount> {
    let value: LastOrCount;

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getcount_cauhoidethi_true($1);",
        values: [this.dethi_id],
      });

      // result row to value
      value = result.rows[0];
    } catch (error) {
      // Return undefined
      console.log(error);
      return undefined;
    }

    return value;
  }
}

export default ServiceCauHoiDeThi;
