import { QueryResult } from "pg"

import pool from "../config/database"
import { ArrayCauTraLoi, CauTraLoi, OnlyCauTraLoi } from "../models/cautraloi"
import { LastOrCount } from "../models/alltypes"

class ServiceCauTraLoi {
  cautraloi_id!: number
  cauhoi_id!: number
  cautraloi_name!: string

  async insert(): Promise<boolean> {
    try {
      // Execute SQL
      await pool.query({
        text: "CALL insert_cautraloi($1, $2);",
        values: [this.cauhoi_id, this.cautraloi_name],
      })
    } catch (error) {
      // Return undefined
      console.log(error)
      return false
    }

    return true
  }

  async update(): Promise<boolean> {
    try {
      // Execute SQL
      await pool.query({
        text: "CALL update_cautraloi($1, $2, $3);",
        values: [this.cautraloi_id, this.cauhoi_id, this.cautraloi_name],
      })
    } catch (error) {
      // Return undefined
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      // Execute SQL
      await pool.query({
        text: "CALL delete_cautraloi($1);",
        values: [this.cautraloi_id],
      })
    } catch (error) {
      // Return undefined
      console.log(error)
      return false
    }

    return true
  }

  async get(): Promise<OnlyCauTraLoi>
  {
    let value: OnlyCauTraLoi

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_cautraloi_withid($1);",
        values: [this.cautraloi_id],
      })

      // result row to value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async getAll(limit: number, offset: number): Promise<ArrayCauTraLoi>
  {
    let allValues: ArrayCauTraLoi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_cautraloi_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: CauTraLoi) => ({
        cautraloi_id: row.cautraloi_id,
        cauhoi_id: row.cauhoi_id,
        cautraloi_name: row.cautraloi_name
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllOnCauHoi(): Promise<ArrayCauTraLoi>
  {
    let allValues: ArrayCauTraLoi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_cautraloi_on_cauhoi($1);",
        values: [this.cauhoi_id],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: CauTraLoi) => ({
        cautraloi_id: row.cautraloi_id,
        cauhoi_id: row.cauhoi_id,
        cautraloi_name: row.cautraloi_name
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getCount(): Promise<LastOrCount>
  {
    let value: LastOrCount

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getcount_cautraloi();"
      })

      // Get count
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async searchAll(keyword: string, limit: number, offset: number): 
    Promise<ArrayCauTraLoi>
  {
    let allValues: ArrayCauTraLoi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM searchall_cautraloi($1, $2, $3);",
        values: [keyword as string, limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: CauTraLoi) => ({
        cautraloi_id: row.cautraloi_id,
        cauhoi_id: row.cauhoi_id,
        cautraloi_name: row.cautraloi_name
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceCauTraLoi