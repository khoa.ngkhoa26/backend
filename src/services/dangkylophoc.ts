import { QueryResult } from "pg"
import pool from "../config/database"
import { ArrayDangKyLopHoc, DangKyLopHoc } from "../models/dangkylophoc"

class ServiceDangKyLopHoc {
  dangkylophoc_id!: number
  sinhvien_id!: number
  lophoc_id!: number

  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_dangkylophoc($1, $2);",
        values: [
          this.sinhvien_id,
          this.lophoc_id
        ]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async update(): Promise<boolean> {
    try {
      if (this.dangkylophoc_id !== undefined)
        await pool.query({
          text: "CALL update_dangkylophoc($1, $2, $3);",
          values: [
            this.dangkylophoc_id,
            this.sinhvien_id,
            this.lophoc_id
          ]
        })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }


  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_dangkylophoc($1);",
        values: [this.dangkylophoc_id]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async getAll(limit: number, offset: number): Promise<ArrayDangKyLopHoc> {
    let allValues: ArrayDangKyLopHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dangkylophoc_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: DangKyLopHoc) => ({
        dangkylophoc_id: row.dangkylophoc_id,
        sinhvien_id: row.sinhvien_id,
        lophoc_id: row.lophoc_id
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceDangKyLopHoc