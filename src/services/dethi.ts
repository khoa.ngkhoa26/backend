import { QueryResult } from "pg";

import pool from "../config/database";
import { ArrayDeThi, DeThi } from "../models/dethi";
import { LastOrCount } from "../models/alltypes";
import { OnlyDeThiMonHoc } from "../models/dethimonhoc";

class ServiceDeThi {
  dethi_id!: number;
  dethimonhoc_id!: number;
  sinhvien_id!: number;
  dethi_startdate!: string;
  dethi_enddate!: string;

  /* Các method cập nhập dữ liệu */
  async insertThu(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_dethithu_random($1, $2);",
        values: [this.dethimonhoc_id, this.sinhvien_id],
      });
    } catch (error) {
      console.log(error);
      return false;
    }

    return true;
  }

  async insertChinh(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_dethichinh_random($1, $2);",
        values: [this.dethimonhoc_id, this.sinhvien_id],
      });
    } catch (error) {
      console.log(error);
      return false;
    }

    return true;
  }

  async getAll(limit: number, offset: number): Promise<ArrayDeThi> {
    let allValues: ArrayDeThi = [];

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethi_withlimitoffset($1, $2);",
        values: [limit, offset],
      });

      // Convert result row to DeThi
      allValues = result.rows.map((row: DeThi) => ({
        dethi_id: row.dethi_id,
        dethimonhoc_id: row.dethimonhoc_id,
        sinhvien_id: row.sinhvien_id,
        dethi_startdate: row.dethi_startdate,
        dethi_enddate: row.dethi_enddate,
      }));
    } catch (error) {
      // Return undefined
      console.log(error);
      return undefined;
    }

    return allValues;
  }

  async getAllOnMonHoc(
    id: number,
    limit: number,
    offset: number
  ): Promise<ArrayDeThi> {
    let allValues: ArrayDeThi = [];

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethi_on_monhoc($1, $2, $3);",
        values: [id, limit, offset],
      });

      // Convert result row to DeThi
      allValues = result.rows.map((row: DeThi) => ({
        dethi_id: row.dethi_id,
        dethimonhoc_id: row.dethimonhoc_id,
        sinhvien_id: row.sinhvien_id,
        dethi_startdate: row.dethi_startdate,
        dethi_enddate: row.dethi_enddate,
      }));
    } catch (error) {
      // Return undefined
      console.log(error);
      return undefined;
    }

    return allValues;
  }

  async getAllOnSinhVien(limit: number, offset: number): Promise<ArrayDeThi> {
    let allValues: ArrayDeThi = [];

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethi_on_sinhvien($1, $2, $3);",
        values: [this.sinhvien_id, limit, offset],
      });

      // Convert result row to DeThi
      allValues = result.rows.map((row: DeThi) => ({
        dethi_id: row.dethi_id,
        dethimonhoc_id: row.dethimonhoc_id,
        sinhvien_id: row.sinhvien_id,
        dethi_startdate: row.dethi_startdate,
        dethi_enddate: row.dethi_enddate,
      }));
    } catch (error) {
      // Return undefined
      console.log(error);
      return undefined;
    }

    return allValues;
  }

  async getAllOnTime(
    time: string,
    limit: number,
    offset: number
  ): Promise<ArrayDeThi> {
    let allValues: ArrayDeThi = [];

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethi_on_time($1, $2, $3);",
        values: [time, limit, offset],
      });

      // Convert result row to DeThi
      allValues = result.rows.map((row: DeThi) => ({
        dethi_id: row.dethi_id,
        dethimonhoc_id: row.dethimonhoc_id,
        sinhvien_id: row.sinhvien_id,
        dethi_startdate: row.dethi_startdate,
        dethi_enddate: row.dethi_enddate,
      }));
    } catch (error) {
      // Return undefined
      console.log(error);
      return undefined;
    }

    return allValues;
  }

  async getCount(): Promise<LastOrCount> {
    let value: LastOrCount;

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getcount_dethi();",
      });

      // result row to value
      value = result.rows[0];
    } catch (error) {
      // Return undefined
      console.log(error);
      return undefined;
    }

    return value;
  }

  async get(): Promise<DeThi | undefined> {
    let value: DeThi;

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_dethi_withid($1);",
        values: [this.dethi_id],
      });

      // result row to value
      value = result.rows[0];
    } catch (error) {
      // Return undefined
      console.log(error);
      return undefined;
    }

    return value;
  }
}

export default ServiceDeThi;
