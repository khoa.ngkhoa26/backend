import { QueryResult } from "pg"

import pool from "../config/database"
import { ArrayNguoiDung, NguoiDung, NguoiDungPassword, OnlyNguoiDung } from "../models/nguoidung"

class ServiceNguoiDung {
  nguoidung_account!: string
  quyen_id!: number
  nguoidung_password!: string
  nguoidung_displayname!: string

  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_nguoidung($1, $2, $3, $4);",
        values: [
          this.nguoidung_account,
          this.quyen_id,
          this.nguoidung_password,
          this.nguoidung_displayname
        ]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async update(): Promise<boolean> {
    try {
      if (this.nguoidung_account !== undefined)
        await pool.query({
          text: "CALL update_nguoidung($1, $2, $3, $4);",
          values: [
            this.nguoidung_account,
            this.quyen_id,
            this.nguoidung_password,
            this.nguoidung_displayname
          ]
        })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async updateWithoutQuyen(): Promise<boolean> {
    try {
      if (this.nguoidung_account !== undefined)
        await pool.query({
          text: "CALL update_nguoidung_without_quyen($1, $2, $3);",
          values: [
            this.nguoidung_account,
            this.nguoidung_password,
            this.nguoidung_displayname
          ]
        })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_nguoidung($1);",
        values: [this.nguoidung_account]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async get(): Promise<OnlyNguoiDung> {
    let value: OnlyNguoiDung

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_nguoidung_withaccount($1);",
        values: [this.nguoidung_account],
      })

      // result row to value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async getWithoutDisplayname(): Promise<void> {
    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_nguoidung_withaccount($1);",
        values: [this.nguoidung_account],
      })

      // result row to service attribute itself
      this.nguoidung_account = result.rows[0].nguoidung_account
      this.quyen_id = result.rows[0].quyen_id
      this.nguoidung_password = result.rows[0].nguoidung_password
    } catch (error) {
      // Return undefined
      console.log(error)
    }
  }

  async getAll(limit: number, offset: number): Promise<ArrayNguoiDung>
  {
    let allValues: ArrayNguoiDung = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_nguoidung_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: NguoiDung) => ({
        nguoidung_account: row.nguoidung_account,
        quyen_id: row.quyen_id,
        nguoidung_password: row.nguoidung_password,
        nguoidung_displayname: row.nguoidung_displayname,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getPassword(): Promise<string | undefined> {
    let value: NguoiDungPassword

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_nguoidung_password($1);",
        values: [this.nguoidung_account],
      })

      // result row to value
      value = result.rows[0].nguoidung_password
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }
}

export default ServiceNguoiDung