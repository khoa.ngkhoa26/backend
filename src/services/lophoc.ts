import { QueryResult } from "pg"

import pool from "../config/database"
import { ArrayLopHoc, LopHoc, OnlyLopHoc } from "../models/lophoc"
import { LastOrCount } from "../models/alltypes"

class ServiceLopHoc {
  lophoc_id!: number
  lophoc_name!: string

  /* Các method cập nhập dữ liệu */
  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_lophoc($1);",
        values: [this.lophoc_name]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async update(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL update_lophoc($1, $2);",
        values: [this.lophoc_id, this.lophoc_name]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_lophoc($1);",
        values: [this.lophoc_id]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  /* Các method lấy dữ liệu */
  async get():
    Promise<OnlyLopHoc>
  {
    let value: OnlyLopHoc

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_lophoc_withid($1);",
        values: [this.lophoc_id],
      })

      // result row to value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async getAll(limit: number, offset: number): Promise<ArrayLopHoc>
  {
    let allValues: ArrayLopHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_lophoc_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to LopHoc and add to allValues
      allValues = result.rows.map((row: LopHoc) => ({
        lophoc_id: row.lophoc_id,
        lophoc_name: row.lophoc_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getCountMonhoc(): Promise<LastOrCount>
  {
    let value: LastOrCount

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getcount_lophoc_on_monhoc($1);",
        values: [this.lophoc_id]
      })

      // Lấy giá trị đếm
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async getCountSinhvien(): Promise<LastOrCount>
  {
    let value: LastOrCount

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getcount_lophoc_on_sinhvien($1);",
        values: [this.lophoc_id]
      })

      // Lấy giá trị đếm
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async getOnSinhvien(sinhvien_id: number):
    Promise<OnlyLopHoc>
  {
    let value: OnlyLopHoc

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_lophoc_on_sinhvien($1);",
        values: [sinhvien_id],
      })

      // result row to value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async searchAll(keyword: string, limit: number, offset: number): Promise<ArrayLopHoc>
  {
    let allValues: ArrayLopHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM searchall_lophoc($1, $2, $3);",
        values: [keyword, limit, offset],
      })

      // Convert result row to LopHoc and add to allValues
      allValues = result.rows.map((row: LopHoc) => ({
        lophoc_id: row.lophoc_id,
        lophoc_name: row.lophoc_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceLopHoc