export type SinhVien = {
  sinhvien_id: number
  nguoidung_account: string
  sinhvien_name: string
  sinhvien_gender: string
  sinhvien_birthday: string
}

export type OnlySinhVien = SinhVien | undefined

export type ArraySinhVien = SinhVien[] | undefined