export type LopHoc = {
  lophoc_id: number
  lophoc_name: string
}

export type OnlyLopHoc = LopHoc | undefined

export type ArrayLopHoc = LopHoc[] | undefined