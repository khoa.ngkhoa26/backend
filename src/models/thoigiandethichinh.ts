export type ThoiGianDeThiChinh = {
  thoigiandethichinh_id: number
  dethimonhoc_id: number
  thoigiandethichinh_time: string
}

export type ArrayThoiGianDeThiChinh = ThoiGianDeThiChinh[] | undefined