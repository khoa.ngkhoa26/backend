export type Quyen = {
  quyen_id: number
  quyen_name: string
}

export type OnlyQuyen = Quyen | undefined

export type ArrayQuyen = Quyen[] | undefined