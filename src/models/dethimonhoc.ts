export type DeThiMonHoc = {
  dethimonhoc_id: number
  monhoc_id: number
  dethimonhoc_name: string
  dethimonhoc_real: boolean
  dethimonhoc_questions: number
  dethimonhoc_time: number
}

export type OnlyDeThiMonHoc = DeThiMonHoc | undefined

export type ArrayDeThiMonHoc = DeThiMonHoc[] | undefined